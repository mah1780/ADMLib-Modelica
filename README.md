# ADMLib

## Description

This is a library for modeling and simulating anaerobic digestion systems in Modelica.

## Documentation

All documentation is included inside the library in HTML format. You can find more information about how to use ADMLib [here](https://doi.org/10.1177%2F00375497211014935).
