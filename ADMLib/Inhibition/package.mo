within ADMLib;
package Inhibition "A package for representing inhibition processes"
  extends Modelica.Icons.Package;
  
  model NonCompetitive "Non competitive inhibition process"
  ADMLib.Types.Concentration kI; ADMLib.Types.InhibitionFunction If; ADMLib.Types.Concentration CI;
  equation
  If = 1/(1+(CI/kI));
  end NonCompetitive;
 
  model SecSubstrate "Inhibition process when there is a secondary substrate"
  ADMLib.Types.Concentration kI; ADMLib.Types.InhibitionFunction If; ADMLib.Types.Concentration CI;
  equation
  If = 1/(1+(kI/CI));
  end SecSubstrate;

  model UpperLowerpH "Empirical upper and lower pH inhibition"
  ADMLib.Types.InhibitionFunction If; ADMLib.Types.pH pH,pHLL,pHUL;
  equation
  If = (1 + 2 * (10^(0.5 * (pHLL - pHUL))))/(1 + (10^(pH - pHUL)) + (10^(pHLL - pH))); 
  end UpperLowerpH;
  
  model LowerpH "Empirical lower pH inhibition only"
  ADMLib.Types.InhibitionFunction If; ADMLib.Types.pH pH,pHLL,pHUL;
  equation
  if pH < pHUL then
  If = exp(- 3 * (((pH - pHUL)/(pHUL - pHLL))^2));
  else
  If = 1;
  end if;
  end LowerpH;

  model HyperbolicTangentpH "Hyperbolic tangent function pH inhibition"
  ADMLib.Types.InhibitionFunction If; ADMLib.Types.pH pH,pHLL,pHUL; ADMLib.Types.Constant phi; ADMLib.Types.Constant a;
  equation
  If = (1/2)*(1+tan(a*phi)); phi = (pH - (pHLL+pHUL)/2)/((pHLL+pHUL)/2);
  end HyperbolicTangentpH;
  
  model HillpH "Hill function for pH inhibition"
  ADMLib.Types.InhibitionFunction If; ADMLib.Types.pH pH,pHLL,pHUL,KpH; ADMLib.Types.Constant n;
  equation
  If = (pH^n)/((pH^n)+(KpH^n)); KpH = (pHLL+pHUL)/2;
  end HillpH;

  model HillHydroniumBasedpH "Hydronium based Hill function for pH inhibition"
  ADMLib.Types.InhibitionFunction If; ADMLib.Types.pH pHLL,pHUL,alpha; ADMLib.Types.Concentration CHplus,KpH; ADMLib.Types.Constant n;
  equation
  If = (KpH^n)/((CHplus^n)+(KpH^n)); KpH = 10^(-(pHLL+pHUL)/2); n = alpha/(pHUL-pHLL);
  end HillHydroniumBasedpH;

  annotation(
    Icon(graphics = {Line(origin = {0.200382, 54.2421}, points = {{7.135, -26}, {22.135, -10}, {47.135, -4}}, thickness = 0.75, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15, smooth = Smooth.Bezier), Ellipse(origin = {-3, 18}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, -2}}, endAngle = 360), Ellipse(origin = {-43, -70}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, -2}}, endAngle = 360), Ellipse(origin = {-13, -74}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, -2}}, endAngle = 360), Ellipse(origin = {11, -60}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, -2}}, endAngle = 360), Ellipse(origin = {-33, 20}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {-57, 8}, fillColor = {133, 50, 14}, fillPattern = FillPattern.Sphere, lineThickness = 0.5, extent = {{-13, 12}, {59, -60}}, endAngle = 360), Ellipse(origin = {-59, 6}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {-71, -22}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {-65, -50}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {25, -34}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {19, -4}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Line(origin = {78.3321, 56.3895}, points = {{-26.9881, 8.63409}, {7.01193, -15.3659}}, color = {239, 41, 41}, thickness = 3), Line(origin = {95.2779, 64.7838}, rotation = -90, points = {{-6.98807, -15.3659}, {29.0119, -39.3659}}, color = {239, 41, 41}, thickness = 3)}, coordinateSystem(initialScale = 0.1)));
end Inhibition;