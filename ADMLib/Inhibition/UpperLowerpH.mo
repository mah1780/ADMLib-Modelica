within ADMLib.Inhibition;

model UpperLowerpH
  ADMLib.Types.InhibitionFunction If;
  ADMLib.Types.pH pH;
  ADMLib.Types.pH pHLL = 1;
  ADMLib.Types.pH pHUL = 1;
equation
  If = (1 + 2 * 10 ^ (0.5 * (pHLL - pHUL))) / (1 + 10 ^ (pH - pHUL) + 10 ^ (pHLL - pH));
end UpperLowerpH;