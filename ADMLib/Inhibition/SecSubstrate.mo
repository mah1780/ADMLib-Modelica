within ADMLib.Inhibition;

model SecSubstrate
  ADMLib.Types.Concentration kI = 1;
  ADMLib.Types.InhibitionFunction If;
  ADMLib.Types.Concentration CI;
equation
  If = 1 / (1 + kI / CI);
end SecSubstrate;