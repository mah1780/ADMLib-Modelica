within ADMLib.Inhibition;

model NonCompetitive
  ADMLib.Types.Concentration kI = 1;
  ADMLib.Types.InhibitionFunction If;
  ADMLib.Types.Concentration CI;
equation
  If = 1 / (1 + CI / kI);
end NonCompetitive;