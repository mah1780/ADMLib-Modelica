package ADMLib "ADMLib - version 1.0.0"
  extends Modelica.Icons.Package;

  package UsersGuide
    extends Modelica.Icons.Information;
    
    class Overview
    extends Modelica.Icons.Information;
    annotation(
        Icon(coordinateSystem(initialScale = 0.1)),
        preferedView = "info",
        Documentation(info = "<html><head></head><body>Essentials to understand how <b>ADMLib</b> works. <p></p>
  <u>
  Basics of mass balances
  </u>
  <p>
  In <b>ADMLib</b>, processes (reactions, mass transfer, inputs/outputs, etc.) are seen as separated components, and substances are linked to them by connectors. <b>Concentration</b> is taken as the <b>potential variable</b> for a <a href=\"modelica://ADMLib.Interfaces.ProcessPort\">ProcessPort</a> connector and <b>concentration variation rates</b> are the respective <b>flow variables</b>.
  </p>
  <p>
  Processes have rates which are related with different species concentrations. For example, consider the following equation that describes the dynamics of some substance inside the digester: 
  </p>
  <p>
  <img src=\"modelica://ADMLib/Resources/Scheme1.png\">
  </p>
  The compound with concentration <i>S(t)</i> is connected to three processes or components: The <b>input/output</b> that represents the inflow and outflow of this particular substance to the system, a <b>first order process,</b> and a <b>biochemical reaction</b> (see <a href=\"modelica://ADMLib.MassBalance.InletOutlet\">InletOutlet</a>, <a href=\"modelica://ADMLib.MassBalance.FirstOrder\">FirstOrder</a> and <a href=\"modelica://ADMLib.MassBalance.GrowthModels\">GrowthModels</a>). Depending on the species, other processes as liquid/gas transfer or acid/base interactions could be added to the equation (see <a href=\"modelica://ADMLib.GasPhase\">GasPhase</a> and <a href=\"modelica://ADMLib.AcidBase.FirstOrder\">AcidBase</a>). 
  <p>
  The sign of the equation indicates if the compound is being consumed or produced in the process. For this library, a substance must be considered as a <b>product</b> of the given process if a <b>positive</b> sign precedes the mathematical term, and a <b>reactant</b> if the sign is <b>negative</b>. This is important when deciding which library component will represent the process in question, since components are included for different quantities of products and reactants.
  </p>
  <p>
  <img src=\"modelica://ADMLib/Resources/Scheme2.png\">
  </p>
  <u>
  Modeling the variation of temperature in a vessel</u><p>
  For a heat connector, a <a href=\"modelica://ADMLib.Interfaces.TemperaturePort\">TemperaturePort</a>, the&nbsp;<b>effort</b> and <b>flow variables</b> are <b>temperature</b> and <b>temperature variation rate</b>. To model energy balances with <b>ADMLib</b>, the approach is analogue to the mass balance equations where a substance, a vessel temperature in this case, is connected to different processes.  
  </p>
  <u>
  Adding inhibitors effect
  </u>
  <p>
  Inhibition factors are included by default in every biochemical component. If a particular reaction is inhibited by one or more factors, the effect could be modeled for each one using the <a href=\"modelica://ADMLib.Inhibition\">Inhibition</a>  sublibrary and added as array elements. Array dimension has to be passed as a parameter and the values of inhibition factors must be connected to a respective array position or directly assigned as 1 for the case with no inhibition considered. 
  </p>
  <u>
  Biochemical processes with two substrates
  </u>
  <p>
  In the case where two compounds could be consumed by the same bacterial group, special components are included. It could be necessary to assign the stoichiometric coefficient of the secondary substrate as 0 to avoid the effect of the process in its dynamics.  
  </p>
        
  </body></html>"));
    end Overview;
    
    class Contact
    extends Modelica.Icons.Contact;
    annotation(
        Icon(coordinateSystem(initialScale = 0.1)),
        preferedView = "info",
        Documentation(info = "<html><head></head><body><b>ADMLib</b> and all its sublibraries had been developed with the resources of the <a href=\"http://www.cenat.ac.cr/en/cnca-en/\">National Advanced Computing Laboratory </a> of the Costa Rica National High Technology Center. Packages and models were implemented by:
    <p>   
    </p><dd><b>Mariela Abdalah</b></dd>
    <dd>Chemical Engineer &amp; Researcher </dd>
    <dd>National Advanced Computing Laboratory</dd>
    <dd>Pavas, Costa Rica</dd>
    <dd>email: <a href=\"mailto:mabdalah@cenat.ac.cr\">mabdalah@cenat.ac.cr</a></dd>
    <dd>&nbsp;&nbsp;</dd>
    <p></p> 
        
  </body></html>"));
    end Contact;
    
    class References
    extends Modelica.Icons.References;
    annotation(Icon(coordinateSystem(initialScale = 0.1)),
        preferedView = "info",
        Documentation(info = "<html>                                                                                                                                    
    <table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
        <tr>
          <td valign=\"top\">[Alatiqi90]</td>
          <td valign=\"top\">I. Alatiqi, A. Dadkhah, and N. Jabr,
            &quot;Dynamics and Multivariable Control Analysis for Anaerobic Digestion,&quot;
            <i>The Chemical Engineering Journal</i>,
            vol. 43, no. 3, pp. 81-91, May. 1990.</td>
        </tr>
        <tr>
          <td valign=\"top\">[Batstone02]</td>
          <td valign=\"top\">D. J. Batstone et al., 
          <i>Anaerobic Digestion Model No. 1</i>. London: IWA Publishing, 2002.</td>
        </tr>
        <tr>
          <td valign=\"top\">[Borisov16]</td>
          <td valign=\"top\">M. Borisov, N. Dimitrova, and I. Simeonov,
          &quot;Mathematical Modelling of Anaerobic Digestion with Hydrogen and Methane Production,&quot;
          <i>IFAC-PapersOnLine</i>,
          vol. 49, no. 26, pp. 231-238, Oct. 2016.</td>
        </tr>
        <tr>
          <td valign=\"top\">[Fritzon15]</td>
          <td valign=\"top\">P. Fritzon,
          <i>Principles of Object Oriented Modeling and Simulation with Modelica 3.3: A Cyber-Physical Approach</i>. New Jersey: Wiley-IEEE Press, 2015.</td>
        </tr>
        <tr>
          <td valign=\"top\">[Gernaey14]</td>
          <td valign=\"top\">K. Gernaey, U. Jeppsson, P. Vanrolleghem, and J. Copp,
          <i>Benchmarking of Control Strategies for Wastewater Treatment Plants</i>. London: IWA Publishing, 2014.</td>
        </tr>
        <tr>
          <td valign=\"top\">[Tiller01]</td>
          <td valign=\"top\">M. M. Tiller,
          <i>Introduction to physical modelling with Modelica</i>. New York: Springer Science + Business Media, 2001.</td>
        </tr>
        <tr>
          <td valign=\"top\">[Wiechert10]</td>
          <td valign=\"top\">W. Wiechert, S. Noack, and A. Elsheikh, <i>Advances in Biochemical Engineering/Biotechnology</i>,
          vol. 121. Berlin: Springer-Verlag, 2010, pp. 109-138. </td>
        </tr>    
    </table>
  </html>"));
    end References;
    
    class ReleaseNotes "Version 1.0.0 (February, 2020)"
    extends Modelica.Icons.ReleaseNotes;
    annotation(
        Icon(coordinateSystem(initialScale = 0.1)),
        preferedView = "info",
        Documentation(info = "<html><head></head><body>This first version was developed and tested with <b>OpenModelica 1.14.1</b> for <b>Linux</b> and <b>Windows,</b> using the old frontend. Compatibility with other software and/or version is not guaranteed.
    </body></html>"));
    end ReleaseNotes;
   
    annotation(
      Icon(coordinateSystem(initialScale = 0.1)),
      preferedView = "info",
      Documentation(info = "<html><head></head><body>ADMLib contains the following modeling subpackages:
  <p></p>
  <p>
  <a href=\"modelica://ADMLib.Types\">Types</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/Types.svg\">
  <p>
  Contains type declarations for variables that are commonly used in anaerobic digestion models.
  </p>
  <p>
  <a href=\"modelica://ADMLib.Interfaces\">Interfaces</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/Interfaces.svg\">
  <p>
  Connectors and partial models for general use are defined in this package. 
  </p>
  <p>
  <a href=\"modelica://ADMLib.MassBalance\">MassBalance</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/MassBalance.svg\">
  <p>
  Contains reaction kinetics of chemical and biochemical processes, and transport of substances inside and outside the system. Includes processes with first order kinetics, bacterial growth models, and input/output expressions (see <a href=\"modelica://ADMLib.MassBalance.FirstOrder\">FirstOrder</a>, <a href=\"modelica://ADMLib.MassBalance.GrowthModels\">GrowthModels</a> and <a href=\"modelica://ADMLib.MassBalance.InletOutlet\">InletOutlet</a>, respectively).
  </p>
  <p>
  <a href=\"modelica://ADMLib.AcidBase\">AcidBase</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/AcidBase.svg\">
  <p>
  Includes several functions for calculating hydronium concentration and for describing acid-base interactions.
  </p>
  <p>
  <a href=\"modelica://ADMLib.Inhibition\">Inhibition</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/Inhibition.svg\">
  <p>
  A compilation of different models to represent inhibition mechanisms.
  </p>
  <p>
  <a href=\"modelica://ADMLib.GasPhase\">GasPhase</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/GasPhase.svg\">
  <p>
  Incorporates some models and equations that are commonly used in the representation of the gas phase behavior in a digester.
  </p>
  <p>
  <a href=\"modelica://ADMLib.HeatTransfer\">HeatTransfer</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/HeatTransfer.svg\">
  <p>
  Necessary to approach the process thermodynamics and the effect of temperature.
  </p>
  <p>
  <a href=\"modelica://ADMLib.Substances\">Substances</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/Substances.svg\">
  <p>
  A simple package to describe the particular role of a substance in the system.
  </p>
  <p>
  <a href=\"modelica://ADMLib.Examples\">Examples</a>
  </p>
  <img src=\"modelica://ADMLib/Resources/Examples.svg\">
  <p>
  Some examples to demonstrate how the library works.</p>
  <p>
  </p><p>
  </p></body></html>"));
  
  end UsersGuide;





  annotation(
    Icon(coordinateSystem(initialScale = 0.1, grid = {10, 10}), graphics = {Polygon(origin = {-65, 10}, lineColor = {46, 91, 8}, fillColor = {208, 240, 117}, pattern = LinePattern.None, fillPattern = FillPattern.Sphere, points = {{-5, 10}, {-2.2656, 15.5469}, {10.4688, 21.0938}, {35.9375, 32.1875}, {56.875, 24.375}, {48.75, 18.75}, {-9.375, 33.125}, {32.5, 7.5}, {80, 15}, {-72.5, 27.5}, {75, 0}, {15, 0}, {-5, 10}}, smooth = Smooth.Bezier), Text(origin = {-25, 25}, fillPattern = FillPattern.Solid, lineThickness = 0.75, extent = {{-35, 15}, {105, -75}}, textString = "ADMLib", fontSize = 59, fontName = "URW Bookman L"), Polygon(origin = {-75, -20}, lineColor = {46, 91, 8}, fillColor = {208, 240, 117}, pattern = LinePattern.None, fillPattern = FillPattern.Sphere, points = {{-2.5, 17.5}, {5, 0}, {-5, 30}, {5, 40}, {-2.5, 17.5}}, smooth = Smooth.Bezier), Line(origin = {-18.4351, -44.6184}, points = {{-45, 25}, {95, 25}}, thickness = 1)}),
    Diagram(coordinateSystem(extent = {{-100, 100}, {100, -100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {10, 10})),
    preferedView = "info",
    Documentation(info = "<html>
<p>
This is a package for modeling and simulating anaerobic digestion systems with Modelica, based on structured modeling frameworks as the Anaerobic Digestion Model No. 1 [Batstone02].
</p>
<p>
<br>
<center>
<img src=\"modelica://ADMLib/Resources/ADMLib.svg\">
</center>
</p>
<br>
<ul>
<p>
<li>Take a look at the <a href=\"modelica://ADMLib.UsersGuide\">User's Guide</a> to learn more about sublibraries.
</p>
<p>
<li>It is highly recommended to read the <a href=\"modelica://ADMLib.UsersGuide.Overview\">Overview</a> before getting started.
</p>
<p>
<li>For more information about the author, go to the <a href=\"modelica://ADMLib.UsersGuide.Contact\">Contact</a> section.
</p>
<p>
<li> Some references used to develop this library are listed in <a href=\"modelica://ADMLib.UsersGuide.References\">References</a>.
</p>
<p>
<li> See the <a href=\"modelica://ADMLib.UsersGuide.ReleaseNotes\">Release Notes</a> to know the details of this release.
</p>
</html>"));
end ADMLib;