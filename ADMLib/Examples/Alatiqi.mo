within ADMLib.Examples;

model Alatiqi "Model of a single CSTR digester with temperature effect [Alatiqi90]"
  parameter Modelica.SIunits.Volume V1 = 3000;
  ADMLib.Types.Yield Ys(unit = "m3vss.L/m3.mgCOD") = 5.75e-05;
  ADMLib.Substances.Substance Substrate(c.start = 300, c.unit = "mgCOD/L", r.unit = "mgCOD/L.d");
  ADMLib.Substances.Substance Bacteria(c.start = 0.5, c.unit = "m3vss/m3", r.unit = "m3vss/m3.d");
  ADMLib.Substances.OutputSubstance Methane(Q.unit = "m3/d", G.unit = "m3methane/d");
  ADMLib.HeatTransfer.VesselTemperature T1(T.start = 55, q.unit = "degC/d", T.unit = "degC");
  ADMLib.MassBalance.GrowthModels.Monod Monod(Ks(unit = "mg/L") = 1458, dim = 1, I = {1}, fr(unit = "m3.mgCOD/m3vss.L") = 1 / Ys, fp(unit = "m3methane.d/m3vss") = 3.045, fb(unit = "1") = 1);
  ADMLib.HeatTransfer.SimpleTemperature ST(k.unit = "1/d", T.unit = "degC");
  ADMLib.MassBalance.InletOutlet.InOut SubstrateInOut(V = V1, Q.unit = "m3/d", Sin(unit = "mgCOD/L") = 9000);
  ADMLib.MassBalance.InletOutlet.Out BacteriaOut(V = V1, Q.unit = "m3/d");
  ADMLib.HeatTransfer.InOutHeat TemperatureInOut(V = V1, Q.unit = "m3/d", Tin(unit = "degC") = 30);
  ADMLib.HeatTransfer.ExternalHeatInput Gu(qin(unit = "degC/d") = 2.5);
  Modelica.Blocks.Sources.Step StepQ1(offset(unit = "m3/d") = 300, height(unit = "m3/d") = 30, startTime = 0);
equation
  connect(Methane.S, Monod.P);
  connect(Substrate.S, Monod.R);
  connect(Bacteria.S, Monod.B);
  connect(T1.T, ST.T);
  connect(ST.k, Monod.kmax);
  connect(T1.H, Gu.H);
  connect(Substrate.S, SubstrateInOut.S);
  connect(Bacteria.S, BacteriaOut.S);
  connect(T1.H, TemperatureInOut.H);
  connect(SubstrateInOut.Q, StepQ1.y);
  connect(BacteriaOut.Q, StepQ1.y);
  connect(TemperatureInOut.Q, StepQ1.y);
  connect(Methane.Q, StepQ1.y);
annotation(
    __OpenModelica_commandLineOptions = "--matchingAlgorithm=PFPlusExt --indexReductionMethod=dynamicStateSelection -d=initialization,NLSanalyticJacobian",
    __OpenModelica_simulationFlags(cpu = "()", emit_protected = "()", lv = "LOG_SIMULATION,LOG_STATS", outputFormat = "csv", s = "dassl"));
end Alatiqi;