within ADMLib.AcidBase;

model AcidBaseRate
  extends ADMLib.Interfaces.AcidBaseProcess;
  ADMLib.Types.AcidBaseConstant kAB = 1;
  ADMLib.Types.AcidBaseEquilibriumConstant Ka;
  ADMLib.Types.StoichiometricCoefficient fab;
  ADMLib.Types.Concentration CHplus;
  ADMLib.Types.Concentration S;
equation
  r = kAB * (An.c * (Ka * CHPlus) - Ka * S);
  fa = fab;
end AcidBaseRate;