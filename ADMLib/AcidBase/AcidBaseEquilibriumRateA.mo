within ADMLib.AcidBase;

model AcidBaseEquilibriumRateA
  extends ADMLib.Interfaces.AcidBaseEquilibriumProcess;
  ADMLib.Types.AcidBaseEquilibriumConstant Ka;
  ADMLib.Types.StoichiometricCoefficient feq;
  ADMLib.Types.Concentration CHplus;
  ADMLib.Types.Concentration C;
equation
  r = feq * (Ka / (Ka + CHplus)) * der(C);
  feeq = feq;
end AcidBaseEquilibriumRateA;