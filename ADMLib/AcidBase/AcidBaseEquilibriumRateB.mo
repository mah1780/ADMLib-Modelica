within ADMLib.AcidBase;

model AcidBaseEquilibriumRateB
  extends ADMLib.Interfaces.AcidBaseEquilibriumProcess;
  ADMLib.Types.AcidBaseEquilibriumConstant Ka;
  ADMLib.Types.StoichiometricCoefficient feq;
  ADMLib.Types.Concentration CHplus;
  ADMLib.Types.Concentration C;
equation
  r = feq * (Ka * C / (Ka + CHplus) ^ 2);
  feeq = feq;
end AcidBaseEquilibriumRateB;