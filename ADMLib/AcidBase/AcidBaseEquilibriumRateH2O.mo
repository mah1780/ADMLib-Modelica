within ADMLib.AcidBase;

model AcidBaseEquilibriumRateH2O
  extends ADMLib.Interfaces.AcidBaseEquilibriumProcess;
  ADMLib.Types.AcidBaseEquilibriumConstant Kw;
  ADMLib.Types.StoichiometricCoefficient feq;
  ADMLib.Types.Concentration CHplus;
equation
  St = feq * (Kw / CHplus ^ 2);
  feeq = feq;
end AcidBaseEquilibriumRateH2O;