within ADMLib.AcidBase;

model Hydroxyl
  ADMLib.Types.Concentration CHplus;
  ADMLib.Types.Concentration COHminus;
  ADMLib.Types.Constant KW;
equation
  COHminus = KW / CHplus;
end Hydroxyl;