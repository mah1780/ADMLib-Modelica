within ADMLib.GasPhase;

model LiquidGasTransfer
  extends ADMLib.Interfaces.LiquidGasProcess;
  ADMLib.Types.VelocityConstant kLa;
  ADMLib.Types.HenrysLawConstant KH;
  ADMLib.Types.CODEquivalent eq;
  ADMLib.Types.StoichiometricCoefficient fll, fgg;
  Modelica.SIunits.Pressure Pgas;
equation
  r = kLa * (L.c - eq * KH * Pgas);
  fl = fll;
  fg = fgg;
end LiquidGasTransfer;