within ADMLib.GasPhase;

model LiquidGasTransferThreeParticipants
  extends ADMLib.Interfaces.LiquidGasProcess;
  ADMLib.Types.VelocityConstant kLa;
  ADMLib.Types.HenrysLawConstant KH;
  ADMLib.Types.CODEquivalent eq;
  ADMLib.Types.StoichiometricCoefficient fl, fg;
  ADMLib.Types.Pressure Pgas;
  ADMLib.Types.Concentration CP;
equation
  r = kLa * (CP - eq * KH * Pgas);
  fll = fl;
  fgg = fg;
end LiquidGasTransferThreeParticipants;