within ADMLib.GasPhase;

model VaporPressure
  ADMLib.Types.Pressure a;
  ADMLib.Types.Temperature b;
  ADMLib.Types.Temperature Tbase = 298.15;
  ADMLib.Types.Temperature Top;
  ADMLib.Types.Pressure Pvapor;
equation
  Pvapor = a * exp(b * (1 / Tbase - 1 / Top));
end VaporPressure;