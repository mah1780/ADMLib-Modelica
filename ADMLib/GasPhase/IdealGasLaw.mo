within ADMLib.GasPhase;

model IdealGasLaw
  import Modelica.Constants.R;
  ADMLib.Types.Concentration Sgas;
  ADMLib.Types.CODEquivalent eq;
  Modelica.SIunits.Pressure Pgas;
  Modelica.SIunits.Temperature T;
equation
  Pgas = Sgas * (R * T) / eq;
end IdealGasLaw;