within ADMLib;
package HeatTransfer "A package for modeling heat transfer processes and the effect of temperature"
  extends Modelica.Icons.Package;
  
  model VesselTemperature "A single vessel temperature dynamics"
  extends ADMLib.Interfaces.VesselTemperature;
  equation
  der(T) = q;
  end VesselTemperature;

  model InOutHeat "Input and ouput temperatures"
  extends ADMLib.Interfaces.InOutHeatProcess;
  ADMLib.Types.Volume V; Modelica.Blocks.Interfaces.RealInput Q; Modelica.Blocks.Interfaces.RealInput Tin;
  equation
  q = (Q/V) * (Tin - H.T);
  end InOutHeat;
  
  model ExternalHeatInput "A simple external heat input"
  extends ADMLib.Interfaces.InOutHeatProcess;
  Modelica.Blocks.Interfaces.RealInput qin;
  equation
  q = qin;
  end ExternalHeatInput;
  
  model SimpleTemperature "A simple model for a growth constant variation with temperature [Alatiqi90]"
  ADMLib.Types.Temperature T; ADMLib.Types.VelocityConstant k;
  equation
  k = 0.013 * T - 0.129;
  end SimpleTemperature;

  model vantHoff "van't Hoff equation for constant variation with temperature"
  ADMLib.Types.Constant K; ADMLib.Types.Constant a; ADMLib.Types.Temperature b; ADMLib.Types.Temperature Tbase; ADMLib.Types.Temperature Top;
  equation
  K = a * exp(b*((1/Tbase)-(1/Top)));
  end vantHoff;

  annotation(
    Icon(coordinateSystem(initialScale = 0.1), graphics = {Rectangle(origin = {2, 2}, lineColor = {46, 52, 54}, fillColor = {92, 53, 102}, fillPattern = FillPattern.HorizontalCylinder, lineThickness = 0.75, extent = {{-40, 54}, {40, -54}}, radius = 15), Line(origin = {15.54, 15.74}, points = {{0, 29}, {0, 51}}, color = {255, 0, 0}, thickness = 1, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 10),   Line(origin = {-12.41, -14.48}, rotation = 180, points = {{0, 29}, {0, 51}}, color = {255, 0, 0}, thickness = 1, arrow = {Arrow.Filled, Arrow.None}, arrowSize = 10), Line(origin = {2.98, 0.93}, points = {{11.019, 43.0743}, {-26.981, 35.0743}, {23.019, 27.0743}, {-26.981, 15.0743}, {23.019, 5.07426}, {-26.981, -6.92574}, {23.019, -14.9257}, {-26.981, -26.9257}, {25.019, -34.9257}, {-14.981, -42.9257}}, color = {239, 41, 41}, thickness = 1.5)}));
end HeatTransfer;