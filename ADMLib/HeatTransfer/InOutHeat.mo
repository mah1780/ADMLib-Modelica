within ADMLib.HeatTransfer;

model InOutHeat
  extends ADMLib.Interfaces.InOutHeatProcess;
  Modelica.SIunits.Volume V = 1;
  Modelica.Blocks.Interfaces.RealInput Q;
  Modelica.Blocks.Interfaces.RealInput Tin;
equation
  q = Q / V * (Tin - H1.T);
end InOutHeat;