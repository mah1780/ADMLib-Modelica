within ADMLib.MassBalance;
package InletOutlet "A subpackage to describe input/output processes dynamics"
  extends Modelica.Icons.Package;
  
  model InOut "Process with inlet and outlet" 
  extends ADMLib.Interfaces.InOutProcess;
  ADMLib.Types.Volume V; ADMLib.Types.VolumetricFlow Q; Modelica.Blocks.Interfaces.RealInput Sin;
  equation
  r = (Q/V) * (Sin - S.c);
  end InOut;
  
  model Out "Process with only an outlet"
  extends ADMLib.Interfaces.InOutProcess;
  ADMLib.Types.Volume V; ADMLib.Types.VolumetricFlow Q;
  equation
  r = - (Q/V) * S.c;
  end Out;
  
  annotation(
    Icon(graphics = {Line(origin = {-123.24, 60.65}, points = {{31.135, -60}, {97.135, -60}}, pattern = LinePattern.Dot, thickness = 0.75, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15), Line(origin = {7.6813, 60.12}, points = {{21.135, -60}, {81.135, -60}}, pattern = LinePattern.Dash, thickness = 0.75, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15), Rectangle(origin = {11, -68}, fillColor = {191, 3, 3}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-37, 94}, {17, 42}})}, coordinateSystem(initialScale = 0.1)));
end InletOutlet;