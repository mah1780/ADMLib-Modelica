within ADMLib.MassBalance.InletOutlet;

model Out
  extends ADMLib.Interfaces.InOutProcess;
  ADMLib.Types.Volume V;
  ADMLib.Types.VolumetricFlow Q;
equation
  r = -Q / V * S.c;
end Out;