within ADMLib.MassBalance.FirstOrder;

model FOPTwoRSixP
  extends ADMLib.Interfaces.PTwoRSixP;
  ADMLib.Types.VelocityConstant k;
  ADMLib.Types.StoichiometricCoefficient fr1;
  ADMLib.Types.StoichiometricCoefficient fr2;
  ADMLib.Types.StoichiometricCoefficient fp1;
  ADMLib.Types.StoichiometricCoefficient fp2;
  ADMLib.Types.StoichiometricCoefficient fp3;
  ADMLib.Types.StoichiometricCoefficient fp4;
  ADMLib.Types.StoichiometricCoefficient fp5;
  ADMLib.Types.StoichiometricCoefficient fp6;
equation
  r = k * R1.c;
  frr1 = fr1;
  frr2 = fr2;
  fpp1 = fp1;
  fpp2 = fp2;
  fpp3 = fp3;
  fpp4 = fp4;
  fpp5 = fp5;
  fpp6 = fp6;
end FOPTwoRSixP;