within ADMLib.MassBalance.FirstOrder;

model FirstOrderProcess
  extends ADMLib.Interfaces.Process;
  ADMLib.Types.VelocityConstant k;
  ADMLib.Types.StoichiometricCoefficient fr;
  ADMLib.Types.StoichiometricCoefficient fp;
equation
  r = k * R.c;
  frr = fr;
  fpp = fp;
end FirstOrderProcess;