within ADMLib.MassBalance.FirstOrder;

model FOPTwoRTwoP
  extends ADMLib.Interfaces.PTwoRTwoP;
  ADMLib.Types.VelocityConstant k;
  ADMLib.Types.StoichiometricCoefficient fr1;
  ADMLib.Types.StoichiometricCoefficient fr2;
  ADMLib.Types.StoichiometricCoefficient fp1;
  ADMLib.Types.StoichiometricCoefficient fp2;
equation
  r = k * R1.c;
  frr1 = fr1;
  frr2 = fr2;
  fpp1 = fp1;
  fpp2 = fp2;
end FOPTwoRTwoP;