within ADMLib.MassBalance.FirstOrder;

model FOPTwoP
  extends ADMLib.Interfaces.PTwoP;
  ADMLib.Types.VelocityConstant k;
  ADMLib.Types.StoichiometricCoefficient fr;
  ADMLib.Types.StoichiometricCoefficient fp1;
  ADMLib.Types.StoichiometricCoefficient fp2;
equation
  r = k * R.c;
  frr = fr;
  fpp1 = fp1;
  fpp2 = fp2;
end FOPTwoP;