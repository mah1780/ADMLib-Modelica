within ADMLib.MassBalance.FirstOrder;

model FOPTwoR
  extends ADMLib.Interfaces.PTwoR;
  ADMLib.Types.VelocityConstant k;
  ADMLib.Types.StoichiometricCoefficient fr1;
  ADMLib.Types.StoichiometricCoefficient fr2;
  ADMLib.Types.StoichiometricCoefficient fp;
equation
  r = k * R1.c;
  frr1 = fr1;
  frr2 = fr2;
  fpp = fp;
end FOPTwoR;