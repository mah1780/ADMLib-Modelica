within ADMLib.MassBalance;
package FirstOrder "A subpackage to describe reactions with first order kinetics"
  extends Modelica.Icons.Package;
  
  model FirstOrderProcess "One reactant, one product"
  extends ADMLib.Interfaces.Process;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr,fp;
  equation
  r = k * R.c; frr = fr; fpp = fp;
  end FirstOrderProcess;

  model FOPTwoP "One reactant, two products"
  extends ADMLib.Interfaces.PTwoP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2;
  equation
  r = k * R.c; frr = fr; fpp1 = fp1; fpp2 = fp2;
  end FOPTwoP;

  model FOPThreeP "One reactant, three products"
  extends ADMLib.Interfaces.PThreeP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3;
  equation
  r = k * R.c; frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3;
  end FOPThreeP;
  
  model FOPFourP "One reactant, four products"
  extends ADMLib.Interfaces.PFourP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3,fp4;
  equation
  r = k * R.c; frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4;
  end FOPFourP;
  
  model FOPFiveP "One reactant, five products"
  extends ADMLib.Interfaces.PFiveP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3,fp4,fp5;
  equation
  r = k * R.c; frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5;
  end FOPFiveP;
  
  model FOPSixP "One reactant, six products"
  extends ADMLib.Interfaces.PSixP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3,fp4,fp5,fp6;
  equation
  r = k * R.c; frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6;
  end FOPSixP;
  
  model FOPTwoR "Two reactants, one product"
  extends ADMLib.Interfaces.PTwoR;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp; 
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; fpp = fp;
  end FOPTwoR;
  
  model FOPTwoRTwoP "Two reactants, two products"
  extends ADMLib.Interfaces.PTwoRTwoP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2;
  end FOPTwoRTwoP;
  
  model FOPTwoRThreeP "Two reactants, three products"
  extends ADMLib.Interfaces.PTwoRThreeP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3;
  end FOPTwoRThreeP;
  
  model FOPTwoRFourP "Two reactants, four products"
  extends ADMLib.Interfaces.PTwoRFourP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4;
  end FOPTwoRFourP;
  
  model FOPTwoRFiveP "Two reactants, five products"
  extends ADMLib.Interfaces.PTwoRFiveP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fp5;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5;
  end FOPTwoRFiveP;
  
  model FOPTwoRSixP "Two reactants, six products"
  extends ADMLib.Interfaces.PTwoRSixP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fp5,fp6;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6;
  end FOPTwoRSixP;
  
  model FOPThreeR "Three reactants, one product"
  extends ADMLib.Interfaces.PThreeR;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp = fp;
  end FOPThreeR;
  
  model FOPThreeRTwoP "Three reactants, two products"
  extends ADMLib.Interfaces.PThreeRTwoP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2;
  end FOPThreeRTwoP;
  
  model FOPThreeRThreeP "Three reactants, three products"
  extends ADMLib.Interfaces.PThreeRThreeP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3;
  end FOPThreeRThreeP;
  
  model FOPThreeRFourP "Three reactants, four products"
  extends ADMLib.Interfaces.PThreeRFourP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4;
  end FOPThreeRFourP;
  
  model FOPThreeRFiveP "Three reactants, five products"
  extends ADMLib.Interfaces.PThreeRFiveP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fp5;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5;
  end FOPThreeRFiveP;

  model FOPThreeRSixP "Three reactants, six products"
  extends ADMLib.Interfaces.PThreeRSixP;
  ADMLib.Types.VelocityConstant k; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fp5,fp6;
  equation
  r = k * R1.c; frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6;
  end FOPThreeRSixP;

  annotation(
    Icon(coordinateSystem(initialScale = 0.1), graphics = {Line(origin = {-56.9, 8.22}, points = {{0, 15}, {0, -15}}, thickness = 1.5), Line(origin = {-49.9, 14.22}, points = {{-7, -7}, {-6, 6}, {3, 9}}, thickness = 1.25, smooth = Smooth.Bezier), Line(origin = {-51.78, -14.64}, points = {{5, 14}, {-3, -8}}, thickness = 1.5), Line(origin = {-41.71, -15.18}, points = {{-4, 14}, {4, -8}}, thickness = 1.5), Line(origin = {-45.7787, -15.1755}, points = {{-5, 0}, {3, 0}}, thickness = 1), Line(origin = {-69.9008, 9.22137}, points = {{-7, 0}, {7, 0}}, thickness = 1.25), Line(origin = {-20.9, 2.01}, points = {{-10, 0}, {10, 0}}, thickness = 1.25), Line(origin = {-21.05, 11.48}, points = {{-10, 0}, {10, 0}}, thickness = 1.25), Line(origin = {5.5, 1.67}, points = {{0, 23}, {0, -15}}, thickness = 1.5), Line(origin = {14.17, -3.92}, points = {{-8, 7}, {6, -11}}, thickness = 1.25), Line(origin = {14.03, 10.21}, points = {{-8, -7}, {4, 5}}, thickness = 1.25), Line(origin = {41.3644, -1.62305}, points = {{10.8035, 16.9708}, {5.80351, 23.4708}, {-4.69649, 23.7208}, {-13.1965, 15.9708}, {-15.1965, 5.4708}, {-13.1965, -5.0292}, {-5.1965, -13.0292}, {4.80352, -13.0292}, {10.8035, -7.0292}}, thickness = 1.5, smooth = Smooth.Bezier), Line(origin = {58.15, -14.79}, points = {{5, 14}, {-3, -8}}, thickness = 1.5), Line(origin = {65.21, -14.79}, points = {{-5, 0}, {3, 0}}, thickness = 1.25), Line(origin = {68.21, -14.79}, points = {{-4, 14}, {4, -8}}, thickness = 1.5), Line(origin = {2, -42}, points = {{-74, 0}, {74, 0}}, color = {204, 0, 0}, thickness = 1.5, arrow = {Arrow.None, Arrow.Open}, arrowSize = 13)}));
end FirstOrder;