within ADMLib.MassBalance.GrowthModels;

model MTwoSTwoP
  extends ADMLib.Interfaces.BPTwoSTwoP;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim];
  ADMLib.Types.StoichiometricCoefficient fr1;
  ADMLib.Types.StoichiometricCoefficient fr2;
  ADMLib.Types.StoichiometricCoefficient fp1;
  ADMLib.Types.StoichiometricCoefficient fp2;
  ADMLib.Types.StoichiometricCoefficient fb;
equation
  r = kmax * R1.c / (Ks + R1.c) * (R1.c / (R1.c + R2.c)) * B.c * product(I);
  frr1 = fr1;
  frr2 = fr2;
  fpp1 = fp1;
  fpp2 = fp2;
  fbb = fb;
end MTwoSTwoP;