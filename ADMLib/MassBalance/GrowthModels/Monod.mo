within ADMLib.MassBalance.GrowthModels;

model Monod
  extends ADMLib.Interfaces.BioProcess;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks;
  ADMLib.Types.InhibitionFactor I[dim];
  ADMLib.Types.StoichiometricCoefficient fr;
  ADMLib.Types.StoichiometricCoefficient fp;
  ADMLib.Types.StoichiometricCoefficient fb;
equation
  r = kmax * R.c / (Ks + R.c) * B.c * product(I);
  frr = fr;
  fpp = fp;
  fbb = fb;
end Monod;