within ADMLib.MassBalance.GrowthModels;

model MThreeP
  extends ADMLib.Interfaces.BPTwoP;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim];
  ADMLib.Types.StoichiometricCoefficient fr;
  ADMLib.Types.StoichiometricCoefficient fp1;
  ADMLib.Types.StoichiometricCoefficient fp2;
  ADMLib.Types.StoichiometricCoefficient fp3;
  ADMLib.Types.StoichiometricCoefficient fb;
equation
  r = kmax * R.c / (Ks + R.c) * B.c * product(I);
  frr = fr;
  fpp1 = fp1;
  fpp2 = fp2;
  fpp3 = fp3;
  fbb = fb;
end MThreeP;