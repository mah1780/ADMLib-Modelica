within ADMLib.MassBalance.GrowthModels;

model Haldane
  extends ADMLib.Interfaces.BioProcess;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks;
  ADMLib.Types.Concentration KI;
  ADMLib.Types.StoichiometricCoefficient fr;
  ADMLib.Types.StoichiometricCoefficient fp;
  ADMLib.Types.StoichiometricCoefficient fb;
equation
  r = kmax * R.c / (Ks + R.c + R.c ^ 2 / KI) * B.c;
  frr = fr;
  fpp = fp;
  fbb = fb;
end Haldane;