within ADMLib.Types;

type ProcessRate = Real(final quantity = "Process rate");