within ADMLib;
package Types "A package for regular variables and types"
  extends Modelica.Icons.TypesPackage;
  
  type AcidBaseConstant = Real(final quantity = "Acid base constant", min = 0);
  type AcidBaseEquilibriumConstant = Real(final quantity = "Acid base equilibrium constant", min = 0);
  type CODEquivalent = Real(final quantity = "COD equivalent");
  type CarbonContents = Real(final quantity = "Carbon contents");
  type Concentration  = Real(final quantity = "Concentration");
  type Constant = Real(final quantity = "Constant");
  type HeatRate = Real(final quantity = "Heat rate");
  type HenrysLawConstant = Real(final quantity = "Henry's law constant", min = 0);
  type IdealGasConstant = Real(final quantity = "Ideal gas constant", min = 0);
  type InhibitionFactor = Real(final quantity = "Inhibition factor");
  type InhibitionFunction = Real(final quantity = "Inhibition function");
  type NitrogenContents = Real(final quantity = "Nitrogen contents", min = 0);
  type pH = Real(final quantity = "pH", min= 0);
  type PipeCoefficient = Real(final quantity = "Pipe friction coefficient", min = 0);
  type Pressure = Real(final quantity = "Pressure");
  type ProcessRate = Real(final quantity = "Process rate");
  type StoichiometricCoefficient = Real(final quantity = "Stoichiometric coefficient");
  type Temperature = Real(final quantity = "Temperature");
  type Term = Real(final quantity = "Term");
  type Value = Real(final quantity = "Value");
  type VelocityConstant = Real(final quantity = "Velocity constant", min = 0);
  type Volume = Real(final quantity = "Volume", min = 0);
  type VolumetricFlow = Real(final quantity = "Volumetric flow", min = 0);
  type Yield = Real(final quantity = "Yield", min = 0);
  
  annotation(
   Icon(coordinateSystem(initialScale = 0.1)));
end Types;