within ADMLib.Types;

type Pressure = Real(final quantity = "Pressure");