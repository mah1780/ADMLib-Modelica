within ADMLib.Types;

type VolumetricFlow = Real(final quantity = "Volumetric flow", min = 0);