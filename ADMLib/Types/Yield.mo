within ADMLib.Types;

type Yield = Real(final quantity = "Yield", min = 0);