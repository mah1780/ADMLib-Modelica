within ADMLib.Types;

type InhibitionFactor = Real(final quantity = "Inhibition factor");