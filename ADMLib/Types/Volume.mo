within ADMLib.Types;

type Volume = Real(final quantity = "Volume", min = 0);