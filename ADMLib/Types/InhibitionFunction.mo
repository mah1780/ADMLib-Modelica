within ADMLib.Types;

type InhibitionFunction = Real(final quantity = "Inhibition function");