within ADMLib.Types;

type Temperature = Real(final quantity = "Temperature");