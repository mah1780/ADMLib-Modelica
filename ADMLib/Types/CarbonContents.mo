within ADMLib.Types;

type CarbonContents = Real(final quantity = "Carbon contents", min = 0);