within ADMLib.Types;

type AcidBaseConstant = Real(final quantity = "Acid base constant", min = 0);