within ADMLib.Types;

type HeatRate = Real(final quantity = "Heat rate");