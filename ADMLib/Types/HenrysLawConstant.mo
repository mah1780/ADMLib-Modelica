within ADMLib.Types;

type HenrysLawConstant = Real(final quantity = "Henry's law constant", min = 0);