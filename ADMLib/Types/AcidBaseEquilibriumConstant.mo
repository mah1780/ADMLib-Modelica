within ADMLib.Types;

type AcidBaseEquilibriumConstant = Real(final quantity = "Acid base equilibrium constant", min = 0);