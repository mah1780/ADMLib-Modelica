within ADMLib.Interfaces;

partial model Process
  ProcessPort R, P;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr, fpp;
equation
  R.r = r * frr;
  P.r = -r * fpp;
end Process;