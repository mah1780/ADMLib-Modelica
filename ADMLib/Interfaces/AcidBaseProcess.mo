within ADMLib.Interfaces;

partial model AcidBaseProcess
  ProcessPort An;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient faa;
equation
  An.r = faa * r;
end AcidBaseProcess;