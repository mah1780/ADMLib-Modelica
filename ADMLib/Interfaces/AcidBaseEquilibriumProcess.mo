within ADMLib.Interfaces;

partial model AcidBaseEquilibriumProcess
  ProcessPort A;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient feq;
equation
  A.r = -faa * r;
end AcidBaseEquilibriumProcess;