within ADMLib.Interfaces;

partial model PTwoP
  ProcessPort R, P1, P2;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr, fpp1, fpp2;
equation
  R.r = r * frr;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
end PTwoP;