within ADMLib.Interfaces;

partial model BioProcess
  ProcessPort R, P, B;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr, fpp, fbb;
equation
  R.r = r * frr;
  P.r = -r * fpp;
  B.r = -r * fbb;
end BioProcess;