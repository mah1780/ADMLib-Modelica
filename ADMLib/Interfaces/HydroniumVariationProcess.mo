within ADMLib.Interfaces;

partial model HydroniumVariationProcess
  ProcessPort Hydronium;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.ProcessRate HplusA;
  ADMLib.Types.Constant HplusB;
equation
  Hydronium.r = HplusA / HplusB;
end HydroniumVariationProcess;