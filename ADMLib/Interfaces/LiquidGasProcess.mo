within ADMLib.Interfaces;

partial model LiquidGasProcess
  ProcessPort L, G;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient fll, fgg;
equation
  L.r = fll * r;
  G.r = -fgg * r;
end LiquidGasProcess;