within ADMLib.Interfaces;

partial model BPThreeRFiveP
  ProcessPort R1, R2, R3, P1, P2, P3, P4, B;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr1, frr2, frr3, fpp1, fpp2, fpp3, fpp4, fpp5, fbb;
equation
  R1.r = r * frr1;
  R2.r = r * frr2;
  R3.r = r * frr3;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
  P3.r = -r * fpp3;
  P4.r = -r * fpp4;
  P5.r = -r * fpp5;
  B.r = -r * fbb;
end BPThreeRFiveP;