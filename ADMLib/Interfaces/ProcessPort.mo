within ADMLib.Interfaces;

connector ProcessPort
  ADMLib.Types.Concentration c;
  flow ADMLib.Types.ProcessRate r;
  annotation(
    Icon(graphics = {Ellipse(origin = {-2, 1}, lineColor = {3, 22, 1}, fillColor = {46, 91, 8}, fillPattern = FillPattern.Sphere, lineThickness = 1, extent = {{-58, 59}, {60, -59}}, endAngle = 360)}));
end ProcessPort;