within ADMLib.Interfaces;

connector TemperaturePort
  ADMLib.Types.Temperature T;
  flow ADMLib.Types.HeatRate q;
  annotation(
    Icon(graphics = {Ellipse(origin = {-2, 1}, lineColor = {3, 22, 1}, fillColor = {164, 0, 0}, fillPattern = FillPattern.Sphere, lineThickness = 1, extent = {{-58, 59}, {60, -59}}, endAngle = 360)}, coordinateSystem(initialScale = 0.1)));
end TemperaturePort;