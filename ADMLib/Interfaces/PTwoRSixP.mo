within ADMLib.Interfaces;

partial model PTwoRSixP
  ProcessPort R1, R2, P1, P2, P3, P4, P5, P6;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr1, frr2, fpp1, fpp2, fpp3, fpp4, fpp5, fpp6;
equation
  R1.r = r * frr1;
  R2.r = r * frr2;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
  P3.r = -r * fpp3;
  P4.r = -r * fpp4;
  P5.r = -r * fpp5;
  P6.r = -r * fpp6;
end PTwoRSixP;