within ADMLib.Interfaces;

partial model InOutHeatProcess
  TemperaturePort H;
  ADMLib.Types.HeatRate q;
equation
  H.q = -q;
end InOutHeatProcess;