within ADMLib.Interfaces;

partial model BPThreeP
  ProcessPort R, P1, P2, P3, B;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr, fpp1, fpp2, fpp3, fbb;
equation
  R.r = r * frr;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
  P3.r = -r * fpp3;
  B.r = -r * fbb;
end BPThreeP;