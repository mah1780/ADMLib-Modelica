within ADMLib.Substances;

model OutputSubstance
  extends ADMLib.Interfaces.Substance;
  ADMLib.Types.VolumetricFlow G;
  Modelica.Blocks.Interfaces.RealInput Q;
equation
  der(c) = r;
  G = Q * r;
end OutputSubstance;